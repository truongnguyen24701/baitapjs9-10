var employees = []
const DSNV = "DSNV"
var datastore = window.localStorage.DSNV
if (datastore) {
    employees = JSON.parse(datastore)
    renderDSNV(employees)
}

var indexid = 0
function NhanVien() {
    var taikhoan = document.getElementById("tknv").value
    var hovaten = document.getElementById("name").value
    var email = document.getElementById("email").value
    var password = document.getElementById("password").value
    var datepicker = document.getElementById("datepicker").value
    var luongCB = Number(document.getElementById("luongCB").value)
    var chucvu = document.getElementById("chucvu").value
    var gioLam = Number(document.getElementById("gioLam").value)
    if (!taikhoan || !hovaten || !email || !password || !datepicker || !luongCB || !chucvu || !gioLam) {
        alert('Vui lòng nhập đầy đủ thông tin')
        return
    }
    var tongLuong = 0
    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
    });


    switch (chucvu) {
        case "Giám đốc":
            tongLuong = formatter.format(luongCB * 3 * gioLam)
            break;
        case "Trưởng phòng":
            tongLuong = formatter.format(luongCB * 2 * gioLam)
            break;
        case "Nhân viên":
            tongLuong = formatter.format(luongCB * gioLam)
            break;

        default:
            break;
    }
    var loaiNhanVien = ""

    if (gioLam >= 192) {
        loaiNhanVien = "Nhân viên xuất sắc"
    } else if (gioLam >= 176) {
        loaiNhanVien = "Nhân viên giỏi"
    } else if (gioLam >= 160) {
        loaiNhanVien = "Nhân viên khá"
    } else {
        loaiNhanVien = "Nhân viên trung bình"
    }


    //giới hạn ký tự tài khoản
    if (taikhoan.length <= 3) {
        alert("Vui lòng nhập tài khoản ít nhất có 4 ký tự")
        return
    } else if (taikhoan.length >= 7) {
        alert("Vui lòng nhập tài khoản có ít nhất là 4 - 6 ký tự")
        return
    }

    //kiểm tra họ và tên chỉ là chữ not số
    var numbers = /^[0-9]+$/;
    if (hovaten.match(numbers)) {
        alert("Vui lòng họ và tên nhập bằng chữ")
        return
    }

    //kiểm tra định dạng email
    var mail = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
    if (!email.match(mail)) {
        alert("Vui lòng nhập địa chỉ email")
        return
    }

    // kiểm tra độ dài của mk và đúng yêu cầu
    var mk = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{6,}$/
    if (!password.match(mk)) {
        alert("Vui lòng nhập mật khẩu có ít nhất 6-10 ký tự và ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt")
        return
    }


    //kiểm tra người nhập lương cơ bản
    if (luongCB < 999000) {
        alert("Vui lòng nhập lương cơ bản từ 1 000 000-20 000 000")
        return
    } else if (luongCB > 20000000) {
        alert("Vui lòng nhập lương cơ bản từ 1 000 000-20 000 000")
        return
    }

    //kiểm tra người dùng click chức vụ
    if (chucvu == "Chọn chức vụ") {
        alert("Vui lòng chọn chức vụ của mình")
        return
    }

    //kiểm tra người dùng nhập số giờ làm
    if (gioLam < 80) {
        alert("Vui lòng nhập tối thiểu từ 80 - 200 giờ")
        return
    } else if (gioLam > 200) {
        alert("Vui lòng nhập tối thiểu từ 80 - 200 giờ")
        return
    }

    document.getElementById("form").reset();

    // lưu dữ liệu trong array
    employees.push({
        taikhoan,
        hovaten,
        email,
        password,
        datepicker,
        luongCB,
        chucvu,
        gioLam,
        tongLuong,
        loaiNhanVien,
    });

    renderDSNV(employees);

    var dsnvJSON = JSON.stringify(employees);

    localStorage.setItem(DSNV, dsnvJSON);
}


// in ra table
function renderDSNV() {
    var contentHTML = ""
    for (var index = 0; index < employees.length; index++) {
        var nv = employees[index]
        var contentTR = `<tr>
                        <td>${nv.taikhoan}</td>
                        <td>${nv.hovaten}</td>
                        <td>${nv.email}</td>
                        <td>${nv.datepicker}</td>
                        <td>${nv.chucvu}</td>
                        <td>${nv.tongLuong}</td>
                        <td>${nv.loaiNhanVien}</td>
                        <button class="btn btn-danger" onclick="deletenv(${index})">Xoá</button>
                        <button class="btn btn-success" onclick="editnv(${index})" data-toggle="modal"
                        data-target="#myModal">Sửa</buttondeletedelete>
                        </tr>`
        contentHTML += contentTR;
    }
    document.getElementById("tableDanhSach").innerHTML = contentHTML;

}

// tìm kiếm 
function search() {
    var searchValue = document.getElementById("searchName").value;
    var list = []
    if (search) {
        for (let i = 0; i < employees.length; i++) {
            var epl = employees[i]
            console.log(epl);
            if (epl.loaiNhanVien === searchValue) {
                list.push(epl)
            }
        }
        renderNewDSNV(list)
    }
}

// in ra table search
function renderNewDSNV(newArr) {
    var contentHTML = ""
    for (var i = 0; i < newArr.length; i++) {
        var nv = newArr[i]
        var contentTR = `<tr>
                        <td>${nv.taikhoan}</td>
                        <td>${nv.hovaten}</td>
                        <td>${nv.email}</td>
                        <td>${nv.datepicker}</td>
                        <td>${nv.chucvu}</td>
                        <td>${nv.tongLuong}</td>
                        <td>${nv.loaiNhanVien}</td>
                        <button class="btn btn-danger" onclick="deletenv(${i})">Xoá</button>
                        <button class="btn btn-success" onclick="editnv(${i})" data-toggle="modal"
                        data-target="#myModal">Sửa</buttondeletedelete>
                        </tr>`
        contentHTML += contentTR;
    }
    document.getElementById("tableDanhSach").innerHTML = contentHTML;

}


// delete nhân viên trong bảng table
function deletenv(index) {
    employees.splice(index, 1);
    renderDSNV()
}

//edit nhân viên trong bảng table
function editnv(index) {
    var data = employees[index]
    indexid = index
    document.getElementById("tknv").value = data.taikhoan
    document.getElementById("name").value = data.hovaten
    document.getElementById("email").value = data.email
    document.getElementById("password").value = data.password
    document.getElementById("datepicker").value = data.datepicker
    document.getElementById("luongCB").value = data.luongCB
    document.getElementById("chucvu").value = data.chucvu
    document.getElementById("gioLam").value = data.gioLam
}

//update
function update() {
    var taikhoan = document.getElementById("tknv").value
    var hovaten = document.getElementById("name").value
    var email = document.getElementById("email").value
    var password = document.getElementById("password").value
    var datepicker = document.getElementById("datepicker").value
    var luongCB = Number(document.getElementById("luongCB").value)
    var chucvu = document.getElementById("chucvu").value
    var gioLam = Number(document.getElementById("gioLam").value)
    if (!taikhoan || !hovaten || !email || !password || !datepicker || !luongCB || !chucvu || !gioLam) {
        alert('Vui lòng nhập đầy đủ thông tin')
        return
    }
    var tongLuong = 0
    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
    });


    var loaiNhanVien = ""
    if (gioLam >= 192) {
        loaiNhanVien = "Nhân viên xuất sắc"
    } else if (gioLam >= 176) {
        loaiNhanVien = "Nhân viên giỏi"
    } else if (gioLam >= 160) {
        loaiNhanVien = "Nhân viên khá"
    } else {
        loaiNhanVien = "Nhân viên trung bình"
    }


    switch (chucvu) {
        case "Sếp":
            tongLuong = formatter.format(luongCB * 3 * gioLam)
            break;
        case "Trưởng phòng":
            tongLuong = formatter.format(luongCB * 2 * gioLam)
            break;
        case "Nhân viên":
            tongLuong = formatter.format(luongCB * gioLam)
            break;

        default:
            break;
    }

    //giới hạn ký tự tài khoản
    if (taikhoan.length <= 3) {
        alert("Vui lòng nhập tài khoản ít nhất có 4 ký tự")
        return
    } else if (taikhoan.length >= 7) {
        alert("Vui lòng nhập tài khoản có ít nhất là 4 - 6 ký tự")
        return
    }

    //kiểm tra họ và tên chỉ là chữ not số
    var numbers = /^[0-9]+$/;
    if (hovaten.match(numbers)) {
        alert("Vui lòng họ và tên nhập bằng chữ")
        return
    }

    //kiểm tra định dạng email
    var mail = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
    if (!email.match(mail)) {
        alert("Vui lòng nhập địa chỉ email")
        return
    }

    //kiểm tra độ dài của mk và đúng yêu cầu
    var mk = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{6,}$/
    if (!password.match(mk)) {
        alert("Vui lòng nhập mật khẩu có ít nhất 6-10 ký tự và ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt")
        return
    }


    //kiểm tra người nhập lương cơ bản
    if (luongCB < 999000) {
        alert("Vui lòng nhập lương cơ bản từ 1 000 000-20 000 000")
        return
    } else if (luongCB > 20000000) {
        alert("Vui lòng nhập lương cơ bản từ 1 000 000-20 000 000")
        return
    }

    //kiểm tra người dùng click chức vụ
    if (chucvu == "Chọn chức vụ") {
        alert("Vui lòng chọn chức vụ của mình")
        return
    }

    //kiểm tra người dùng nhập số giờ làm
    if (gioLam < 80) {
        alert("Vui lòng nhập tối thiểu từ 80 - 200 giờ")
        return
    } else if (gioLam > 200) {
        alert("Vui lòng nhập tối thiểu từ 80 - 200 giờ")
        return
    }

    document.getElementById("form").reset();

    employees[indexid] = {
        taikhoan,
        hovaten,
        email,
        password,
        datepicker,
        luongCB,
        chucvu,
        gioLam,
        tongLuong,
        loaiNhanVien,
    }
    renderDSNV()
}

